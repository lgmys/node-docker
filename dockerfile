# multistage build for optimized artifact sizes

# build stage
FROM node:alpine AS build

# Copy related files to the image.
# Note, that we can omit files we dont need by putting them into .dockerignore file
COPY --chown=node:node . /app

# Self explanatory
WORKDIR /app

# Best practice it to run applications as non-root
USER node

# Install dependencies and build the TS
RUN npm i && npm run build

# Specify entry command
CMD npm run dev

# production stage
FROM node:alpine AS production

COPY --chown=node:node --from=build /app/*.json /app/

COPY --chown=node:node --from=build /app/dist /app/dist/

WORKDIR /app

USER node

RUN npm i --only=production

CMD npm start
