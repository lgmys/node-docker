import Koa from 'koa';

const app = new Koa();

const PORT = 3000;

app.use(async (ctx) => {
  ctx.body = 'Hello World';
});

// tslint:disable-next-line:no-console
app.listen(PORT, () => console.log(`app is listening on port ${PORT}`));
