Building images:
docker build . -t sample

Mounting a volume:
docker run -v ~/Code/node-js-docker/src:/app/src sample
